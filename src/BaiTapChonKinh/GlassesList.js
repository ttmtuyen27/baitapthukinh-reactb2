import React, { Component } from "react";

export default class GlassesList extends Component {
  render() {
    return (
      <div className="mx-4">
        {this.props.dataGlasses.map((item) => {
          return (
            <img
              onClick={() => {
                this.props.handleChonKinh(item);
              }}
              style={{ width: "150px", margin: "0 20px", cursor: "pointer" }}
              src={item.virtualGlasses}
              alt=""
            />
          );
        })}
      </div>
    );
  }
}
