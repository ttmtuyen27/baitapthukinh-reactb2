import React, { Component } from "react";

export default class ModelGlasses extends Component {
  render() {
    let glass = this.props.currentGlasses;
    return (
      <div
        style={{
          backgroundImage: `url("./BaitapGlasses/glassesImage/model.jpg")`,
          height: "350px",
          backgroundPosition: "center",
          backgroundSize: "contain",
          backgroundRepeat: "no-repeat",
          position: "relative",
        }}
      >
        <img
          style={{
            width: "165px",
            margin: "90px",
          }}
          src={glass.url}
          alt=""
        />
        {this.props.isOpenInfor && (
          <div
            className="text-center py-2"
            style={{
              width: "287px",
              backgroundColor: "rgba(255,0,0,0.5)",
              height: "120px",
              position: "absolute",
              bottom: "0",
              left: "50%",
              transform: "translate(-50%,0)",
            }}
          >
            <h5
              style={{
                display: "inline",
                marginRight: "50px",
                color: "#3853a4",
                fontWeight: "bold",
              }}
            >
              {glass.name}
            </h5>
            <span className="rounded px-3 py-1 bg-warning">${glass.price}</span>
            <p className="text-light mt-1">{glass.desc}</p>
          </div>
        )}
      </div>
    );
  }
}
// id: 6,
// price: 60,
// name: "PRADA P9700",
// url: "./Bài tập glasses/glassesImage/v6.png",
// desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
// virtualGlasses: "./Bài tập glasses/glassesImage/g6.jpg",
