import React, { Component } from "react";
import { dataGlasses } from "./dataGlasses";
import GlassesList from "./GlassesList";
import ModelGlasses from "./ModelGlasses";

export default class BaiTapChonKinh extends Component {
  state = {
    dataGlasses: dataGlasses,
    currentGlasses: "",
    isOpenInfor: false,
  };
  handleChonKinh = (glass) => {
    this.setState({ currentGlasses: glass, isOpenInfor: true });
  };
  render() {
    return (
      <div className="container py-5">
        <ModelGlasses
          currentGlasses={this.state.currentGlasses}
          isOpenInfor={this.state.isOpenInfor}
        />
        <GlassesList
          dataGlasses={this.state.dataGlasses}
          handleChonKinh={this.handleChonKinh}
        />
      </div>
    );
  }
}
